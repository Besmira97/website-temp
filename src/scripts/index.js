import '../styles/index.scss';

$(document).ready(()=>{
    
    console.log('Hello jQuery');

    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */ 

     /**
     * Documentation for the Lightbox - Fancybox
     * See the section 5. Fire plugin using jQuery selector.
     * http://fancybox.net/howto
     */ 

     /**
      * Boostrap Modal (For the Learn More button)
      * https://getbootstrap.com/docs/4.0/components/modal/
      */
    

});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */ 
window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};

/*
This is for the service section
*/

    //Accordian
    
$('.jquery_accordion_title').click(function() {
        $(this).closest('.jquery_accordion_item').siblings().removeClass('active').find('.jquery_accordion_content').slideUp(400);
        $(this).closest('.jquery_accordion_item').toggleClass('active').find('.jquery_accordion_content').slideToggle(400);
        return false;

        
});



//Testimonials

$(function(){
    $('.bxslider').bxSlider({
      mode: 'fade',
      captions: true,
      slideWidth: 900,
    });
});


//gallery

$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox();
